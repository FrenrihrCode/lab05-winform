﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lab05
{
    public partial class Form1 : Form
    {
        SqlConnection conn;
        DataSet ds;
        DataTable tablePerson = new DataTable();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                //Usar "." o "localhost" para servidores locales, en todo caso
                //se puede usar el nombre del servidor para su conexión si no funciona
                string str = "Server=.;DataBase=School;Integrated Security=true;";
                conn = new SqlConnection(str);
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al conectar:\n"+err.ToString());
            }
        }
        //Funcion para listar personas
        private void btnListar_Click(object sender, EventArgs e)
        {
            /* Lab 05
            try
            {
                //Abrir conexion y ejecutar query
                conn.Open();
                string sql = "SELECT * FROM Person";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader reader = cmd.ExecuteReader();
                //Guardar la info en una tabla
                DataTable dt = new DataTable();
                dt.Load(reader);
                //Colocar los datos en el datagridview
                dgvListado.DataSource = dt;
                dgvListado.Refresh();
                conn.Close();
            } catch (Exception ex)
            {
                conn.Close();
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            } */
            try
            {
                string sql = "SELECT * FROM Person";
                SqlCommand cmd = new SqlCommand(sql, conn);
                //Inicializamos el dataAdapter
                SqlDataAdapter adapter = new SqlDataAdapter();
                ds = new DataSet();
                adapter.SelectCommand = cmd;
                //Llenamos el dataset con la tabla Person
                adapter.Fill(ds, "Person");
                //Asignamos esa tabla del dataset a un objeto table
                //para trabajar directamente con él
                tablePerson = ds.Tables["Person"];
                dgvListado.DataSource = tablePerson;
                Console.WriteLine(tablePerson.Rows.Count);
                dgvListado.Update();
                btnBuscar.Enabled = true;
                btnInsertar.Enabled = true;
                btnEliminar.Enabled = true;
                btnModificar.Enabled = true;
                btnOrdenar.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            /* Lab 05
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                if (firstName != "" && lastName != "")
                {
                    //Abrir conexion y ejecutar query
                    conn.Open();
                    string sp = "InsertPerson";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@FirstName", firstName);
                    cmd.Parameters.AddWithValue("@LastName", lastName);
                    //Insertar valores nulos si se desea
                    if (dtpHireDate.Checked)
                        cmd.Parameters.AddWithValue("@HireDate", dtpHireDate.Value);
                    else
                        cmd.Parameters.AddWithValue("@HireDate", DBNull.Value);
                    if (dtpEnrollmentDate.Checked)
                        cmd.Parameters.AddWithValue("@EnrollmentDate", dtpEnrollmentDate.Value);
                    else
                        cmd.Parameters.AddWithValue("@EnrollmentDate", DBNull.Value);

                    int codigo = Convert.ToInt32(cmd.ExecuteScalar());
                    MessageBox.Show("Se ha registrado nueva persona con el código: " + codigo);
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            } */
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                if (!String.IsNullOrWhiteSpace(firstName) && !String.IsNullOrWhiteSpace(lastName))
                {
                    SqlCommand cmd = new SqlCommand("InsertPerson", conn);

                    cmd.Parameters.Add("@LastName", SqlDbType.VarChar, 50, "LastName");
                    cmd.Parameters.Add("@FirstName", SqlDbType.VarChar, 50, "FirstName");
                    cmd.Parameters.Add("@HireDate", SqlDbType.Date).SourceColumn = "HireDate";
                    cmd.Parameters.Add("@EnrollmentDate", SqlDbType.Date).SourceColumn = "EnrollmentDate";
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.InsertCommand = cmd;
                    adapter.InsertCommand.CommandType = CommandType.StoredProcedure;
                    //Creamos una fila nueva, la cual insertaremos en la BD
                    //Esta fila debe tener la estructura correspondiente
                    DataRow fila = tablePerson.NewRow();
                    fila["LastName"] = lastName;
                    fila["FirstName"] = firstName;
                    //Para trabjar con las fechas nulas, se realizará una validación simple
                    //del check de cada datetimepicker
                    if (dtpHireDate.Checked)
                        fila["HireDate"] = dtpHireDate.Text;
                    else
                        fila["HireDate"] = DBNull.Value;
                    if (dtpEnrollmentDate.Checked)
                        fila["EnrollmentDate"] = dtpEnrollmentDate.Text;
                    else
                        fila["EnrollmentDate"] = DBNull.Value;
                    //Una ves se tiene la fila, la agregamos a la table Person del dataset
                    tablePerson.Rows.Add(fila);
                    //Actualizamos el dataset con la tabla Person
                    adapter.Update(tablePerson);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            /* Lab 05
            try
            {
                string personID = txtPersonID.Text;
                if (personID != "")
                {
                    conn.Open();
                    string sp = "DeletePerson";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PersonID", personID);

                    int res = cmd.ExecuteNonQuery();
                    if (res > 0)
                    {
                        MessageBox.Show("Registro eliminado correctamente");
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            } */
            try
            {
                string personID = txtPersonID.Text;
                if (!String.IsNullOrWhiteSpace(personID))
                {
                    SqlCommand cmd = new SqlCommand("DeletePerson", conn);

                    cmd.Parameters.Add("@PersonID", SqlDbType.VarChar).SourceColumn = "PersonID";

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.DeleteCommand = cmd;
                    adapter.DeleteCommand.CommandType = CommandType.StoredProcedure;
                    //Buscamos la fila a eliminar
                    DataRow[] fila = tablePerson.Select(String.Format("PersonID = '{0}'", personID));
                    //Eliminamos de la tabla la fila especificada
                    tablePerson.Rows.Remove(fila[0]);
                    //Actualizamos el dataset con la tabla modificada
                    adapter.Update(tablePerson);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            /* Lab 05
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string personID = txtPersonID.Text;
                if (firstName != "" && lastName != "" && personID != "")
                {
                    //Abrir conexion y ejecutar query
                    conn.Open();
                    string sp = "UpdatePerson";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PersonID", personID);
                    cmd.Parameters.AddWithValue("@FirstName", firstName);
                    cmd.Parameters.AddWithValue("@LastName", lastName);
                    //Insertar valores nulos si se desea
                    if (dtpHireDate.Checked)
                        cmd.Parameters.AddWithValue("@HireDate", dtpHireDate.Value);
                    else
                        cmd.Parameters.AddWithValue("@HireDate", DBNull.Value);
                    if (dtpEnrollmentDate.Checked)
                        cmd.Parameters.AddWithValue("@EnrollmentDate", dtpEnrollmentDate.Value);
                    else
                        cmd.Parameters.AddWithValue("@EnrollmentDate", DBNull.Value);

                    int res = cmd.ExecuteNonQuery();
                    if (res > 0)
                    {
                        MessageBox.Show("Registro actualizado correctamente");
                    }
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            } */
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string personID = txtPersonID.Text;
                if (!String.IsNullOrWhiteSpace(firstName) && 
                    !String.IsNullOrWhiteSpace(lastName) && !String.IsNullOrWhiteSpace(personID))
                {
                    SqlCommand cmd = new SqlCommand("UpdatePerson", conn);

                    cmd.Parameters.Add("@PersonID", SqlDbType.VarChar).SourceColumn = "PersonID";
                    cmd.Parameters.Add("@LastName", SqlDbType.VarChar).SourceColumn = "LastName";
                    cmd.Parameters.Add("@FirstName", SqlDbType.VarChar).SourceColumn = "FirstName";
                    cmd.Parameters.Add("@HireDate", SqlDbType.Date).SourceColumn = "HireDate";
                    cmd.Parameters.Add("@EnrollmentDate", SqlDbType.Date).SourceColumn = "EnrollmentDate";

                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.UpdateCommand = cmd;
                    adapter.UpdateCommand.CommandType = CommandType.StoredProcedure;
                    //Creamos un array de datarow el cual almacenará la fila que coincida
                    //con el resultado del campo PersonID
                    //A cada campo encontrado le asignamos el valor modificado
                    DataRow[] fila = tablePerson.Select(String.Format("PersonID = '{0}'", personID));
                    fila[0]["LastName"] = lastName;
                    fila[0]["FirstName"] = firstName;
                    //Al igual que en insertar, verificamos el check para evaluar el valor de las fechas
                    if (dtpHireDate.Checked)
                        fila[0]["HireDate"] = dtpHireDate.Text;
                    else
                        fila[0]["HireDate"] = DBNull.Value;
                    if (dtpEnrollmentDate.Checked)
                        fila[0]["EnrollmentDate"] = dtpEnrollmentDate.Text;
                    else
                        fila[0]["EnrollmentDate"] = DBNull.Value;
                    //Actualizamos el dataset con la tabla modificada
                    adapter.Update(tablePerson);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }
        //
        private void dgvListado_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvListado.SelectedRows.Count > 0)
            {
                txtPersonID.Text = dgvListado.SelectedRows[0].Cells[0].Value.ToString();
                txtLastName.Text = dgvListado.SelectedRows[0].Cells[1].Value.ToString();
                txtFirstName.Text = dgvListado.SelectedRows[0].Cells[2].Value.ToString();
                //Capturamos datos de fecha primero en string
                string hireDate = dgvListado.SelectedRows[0].Cells[3].Value.ToString();
                string enrrollmentDate = dgvListado.SelectedRows[0].Cells[4].Value.ToString();
                //Si se encuentra un valor cambiamos el check a true
                dtpHireDate.Checked = !string.IsNullOrWhiteSpace(hireDate);
                dtpEnrollmentDate.Checked = !string.IsNullOrWhiteSpace(enrrollmentDate);

                dtpHireDate.Text = hireDate;
                dtpEnrollmentDate.Text = enrrollmentDate;
            }
        }

        private void dtpEnrollmentDate_ValueChanged(object sender, EventArgs e)
        {
            if (!dtpEnrollmentDate.Checked)
            {
                // Esconder el formato de la fecha
                dtpEnrollmentDate.CustomFormat = " ";
                dtpEnrollmentDate.Format = DateTimePickerFormat.Custom;
            }
            else
            {
                dtpEnrollmentDate.CustomFormat = null;
                dtpEnrollmentDate.Format = DateTimePickerFormat.Short;
            }
        }

        private void dtpHireDate_ValueChanged(object sender, EventArgs e)
        {
            if (!dtpHireDate.Checked)
            {
                // Esconder el formato de la fecha
                dtpHireDate.CustomFormat = " ";
                dtpHireDate.Format = DateTimePickerFormat.Custom;
            }
            else
            {
                dtpHireDate.CustomFormat = null;
                dtpHireDate.Format = DateTimePickerFormat.Short;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            /* Lab 05
            try
            {
                string firstName = txtFirstName.Text;
                string lastName = txtLastName.Text;
                string personID = txtPersonID.Text;
                //Por lo menos un dato escrito
                if (firstName != "" || lastName != "" || personID != "")
                {
                    //Abrir conexion y ejecutar query
                    conn.Open();
                    string sp = "BuscarPersonaByManyCamps";
                    SqlCommand cmd = new SqlCommand(sp, conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    //Añadimos los parametros
                    cmd.Parameters.Add("@PersonID", SqlDbType.Int);
                    cmd.Parameters.Add("@FirstName", SqlDbType.NVarChar);
                    cmd.Parameters.Add("@LastName", SqlDbType.NVarChar);
                    //Verificamos que la variable personID sea un número
                    if (int.TryParse(personID, out int number))
                        cmd.Parameters["@PersonID"].Value = personID;
                    //Si no es número añadimos null
                    else
                        cmd.Parameters["@PersonID"].Value = DBNull.Value;
                    cmd.Parameters["@FirstName"].Value = firstName;
                    cmd.Parameters["@LastName"].Value = lastName;

                    SqlDataReader reader = cmd.ExecuteReader();
                    //Guardar la info en una tabla
                    DataTable dt = new DataTable();
                    dt.Load(reader);
                    //Colocar los datos en el datagridview
                    dgvListado.DataSource = dt;
                    dgvListado.Refresh();
                    conn.Close();
                }
            }
            catch (Exception ex)
            {
                conn.Close();
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }*/
            try
            {
                string firstName = txtFirstName.Text.Trim();
                string lastName = txtLastName.Text.Trim();
                string personID = txtPersonID.Text.Trim();
                //Por lo menos un dato escrito
                if (!String.IsNullOrEmpty(firstName) || !String.IsNullOrEmpty(lastName)
                    || !String.IsNullOrEmpty(personID))
                {
                    string filterStatement = "";
                    if (!String.IsNullOrEmpty(personID))
                    {
                        filterStatement += String.Format("PersonID = '{0}'", personID);
                    }
                    if (!String.IsNullOrEmpty(filterStatement))
                    {
                        filterStatement += " AND ";
                    }
                    filterStatement += String.Format("LastName LIKE '%{0}%'", lastName);
                    if (!String.IsNullOrEmpty(filterStatement))
                    {
                        filterStatement += " AND ";
                    }
                    filterStatement += String.Format("FirstName LIKE '%{0}%'", firstName);
                    Console.WriteLine(filterStatement);
                    DataView dv = new DataView(tablePerson);
                    dv.RowFilter = filterStatement;
                    dgvListado.DataSource = dv;
                }           
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }

        private void btnOrdenar_Click(object sender, EventArgs e)
        {
            try
            {
                DataView dv = new DataView(tablePerson);
                dv.Sort = "LastName ASC";
                dgvListado.DataSource = dv;
            } catch (Exception ex)
            {
                MessageBox.Show("Ocurrió un error:\n" + ex.ToString());
            }
        }
    }
}
